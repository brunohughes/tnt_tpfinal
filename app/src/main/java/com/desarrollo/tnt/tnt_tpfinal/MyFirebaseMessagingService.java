package  com.desarrollo.tnt.tnt_tpfinal;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.bumptech.glide.Glide;
import com.desarrollo.tnt.tnt_tpfinal.util.Utiles;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.concurrent.ExecutionException;

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        // aca no hacemos nada
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData() != null) {

            // recibe accion:ACTUALIZAR
            String accion = remoteMessage.getData().get("accion");

            if( accion.equals("ACTUALIZAR") ){

                // aca rescatamos data que enviamos en la notificacion de google
                // y hacemos algo
                /*String titulo = remoteMessage.getData().get("titulo");
                String texto = remoteMessage.getData().get("texto");
                String fecha_envio = remoteMessage.getData().get("fecha_envio");
                String imagen = remoteMessage.getData().get("imagen");
                String priority = remoteMessage.getData().get("priority");
                */
                // con esta funacio notificamos al sistema
                showNotification();
            }
        }
    }

    private void showNotification() {

        // cargo imagen de la notificacion
        Bitmap bitmap = null;
        try {
            bitmap = Glide
                    .with(getApplicationContext())
                    .asBitmap()
                    .load(R.drawable.yoreparo_logo)
                    .submit()
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // texto expandido
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        String txt = getText(R.string.notificacion_texto).toString();
        bigText.bigText(txt);
        bigText.setBigContentTitle(getText(R.string.notificacion_titulo));
        bigText.setSummaryText(getText(R.string.notificacion_summary));

        // creo el intent
        Intent i = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.setAction("ACTUALIZAR_FCM");

        // creo la falsa pila
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(i);

        Long requestCode = Math.round(Math.random() * 9999);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(requestCode.intValue(),PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this,Utiles.ANDROID_CHANNEL_ID)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(R.drawable.yoreparo_logo)
                        .setLargeIcon(bitmap)
                        .setContentTitle(getText(R.string.notificacion_summary))
                        .setContentText(txt)
                        .setAutoCancel(true)
                        .setSound(defaultSound)
                        .setStyle(bigText);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Utiles.NOTIFICATION_ID, notificationBuilder.build());
    }
}
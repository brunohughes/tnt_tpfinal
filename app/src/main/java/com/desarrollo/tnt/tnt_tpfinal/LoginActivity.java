package com.desarrollo.tnt.tnt_tpfinal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.desarrollo.tnt.tnt_tpfinal.modelos.Usuario;
import com.desarrollo.tnt.tnt_tpfinal.modelos.UsuarioDao;
import com.desarrollo.tnt.tnt_tpfinal.util.App;
import com.desarrollo.tnt.tnt_tpfinal.util.Utiles;

import java.sql.Timestamp;

public class LoginActivity extends AppCompatActivity {

    private EditText nombre;
    private Button btn_enviar;
    private Context contexto = this;
    private Activity actividad = this;
    private ProgressDialog pg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nombre = findViewById(R.id.login_nombre);
        btn_enviar = findViewById(R.id.btn_login);

        btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarLogin();
            }
        });
    }

    private void enviarLogin(){

        if( !TextUtils.isEmpty(nombre.getText()) && nombre.length() > 4 ){

            // cehequeo si hay conexion para acualizar
            if( !(Utiles.conectadoRedMovil(this)) && !(Utiles.conectadoWifi(this))){
                Toast t = Toast.makeText(this,this.getText(R.string.sin_conexion),Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                t.show();
                return;
            }

            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    pg = new ProgressDialog(actividad);
                    pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    pg.setMessage(actividad.getText(R.string.creando_usuario));
                    pg.setCancelable(false);
                    pg.setMax(100);
                    pg.show();
                }

                @Override
                protected Void doInBackground(Void... voids) {

                    // creo nuevo usuario
                    Usuario user =  new Usuario();
                    user.setNombre(nombre.getText().toString());

                    // genero un numero para concatenar
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    Long estampa = timestamp.getTime();

                    // genero el ide del user
                    String SID = Utiles.sha512(nombre.getText().toString() + estampa );
                    user.setSID(SID);

                    // guardo usuario
                    UsuarioDao uDao = ((App)actividad.getApplication()).getDaoSession().getUsuarioDao();
                    uDao.save(user);

                    // guardo los datos en mis preferencias
                    Utiles.setPreference("nombre",nombre.getText().toString(),contexto);
                    Utiles.setPreference("SID",SID,contexto);
                    Utiles.setPreference("estampa",String.valueOf(estampa),contexto);

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    pg.dismiss();

                    // lanzo actividad principal
                    Intent i = new Intent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.setClass(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    actividad.finish();

                }
            }.execute();

        }else{
            Toast t = Toast.makeText(this,this.getText(R.string.error_login),Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
            t.show();
        }
    }

}

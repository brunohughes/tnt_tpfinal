package com.desarrollo.tnt.tnt_tpfinal.clases;

import com.desarrollo.tnt.tnt_tpfinal.modelos.Reparaciones;
import java.util.Comparator;

public class ReparacionesComparator implements Comparator<Reparaciones> {

    @Override
    public int compare(Reparaciones r1, Reparaciones r2){
        Integer id1 = r1.getId_externo();
        Integer id2 = r2.getId_externo();

        if( id2.compareTo(id1) > 0 ){
            return 1;
        }else if( id2.compareTo(id1) < 0){
            return -1;
        }else{
            return 0;
        }
    }
}

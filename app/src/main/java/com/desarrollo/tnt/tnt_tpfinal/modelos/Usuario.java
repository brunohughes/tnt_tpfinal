package com.desarrollo.tnt.tnt_tpfinal.modelos;

import android.app.Activity;

import com.desarrollo.tnt.tnt_tpfinal.util.App;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

import java.io.Serializable;
import java.util.List;

@Entity
public class Usuario  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id(autoincrement = true)
    private Long id;

    private String nombre;
    private String SID;

    public Usuario() {
    }

    public Usuario(String nombre, String sid) {
        this.nombre = nombre;
        this.SID = sid;
    }

    @Generated(hash = 1107984900)
    public Usuario(Long id, String nombre, String SID) {
        this.id = id;
        this.nombre = nombre;
        this.SID = SID;
    }

    public String getSID() {
        return SID;
    }

    public void setSID(String SID) {
        this.SID = SID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public static boolean checkUsuario(Activity actividad){
            UsuarioDao uDao = ((App)actividad.getApplication()).getDaoSession().getUsuarioDao();
            List<Usuario> usrs = uDao.loadAll();
            return usrs.size() != 0;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

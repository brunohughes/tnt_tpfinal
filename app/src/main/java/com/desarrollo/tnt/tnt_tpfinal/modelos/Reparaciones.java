package com.desarrollo.tnt.tnt_tpfinal.modelos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.desarrollo.tnt.tnt_tpfinal.DetalleFragment;
import com.desarrollo.tnt.tnt_tpfinal.MainActivity;
import com.desarrollo.tnt.tnt_tpfinal.R;
import com.desarrollo.tnt.tnt_tpfinal.adaptadores.SimpleItemRecyclerViewAdapter;
import com.desarrollo.tnt.tnt_tpfinal.clases.ReparacionesComparator;
import com.desarrollo.tnt.tnt_tpfinal.util.App;
import com.desarrollo.tnt.tnt_tpfinal.util.Utiles;
import com.desarrollo.tnt.tnt_tpfinal.util.VolleySingleton;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.desarrollo.tnt.tnt_tpfinal.util.Utiles.getPreference;


@Entity
public class Reparaciones implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id(autoincrement = true)
    private Long id;

    private Integer id_externo;
    private String direccion;
    private Double latitud;
    private Double longitud;
    private Integer estado; // 0 - pendiente de atencion | 1 - en proceso | 2 - finalizado
    private String obs;
    private Integer tipo_visita; // 1 - visita | 2 - reparacion
    private String fecha;
    private String empleado;
    private String SID;
    private Boolean geofenceActivate;

    public Reparaciones() {
        this.geofenceActivate = false;
    }

    public Reparaciones(Integer id_externo,String direccion, Double latitud, Double longitud, Integer estado, String obs, Integer tipo_visita, String fecha, String empleado, String SID, Boolean geofenceActivate) {
        this.id_externo = id_externo;
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
        this.estado = estado;
        this.obs = obs;
        this.tipo_visita = tipo_visita;
        this.fecha = fecha;
        this.empleado = empleado;
        this.SID = SID;
        this.geofenceActivate = geofenceActivate;
    }

    @Generated(hash = 1104703711)
    public Reparaciones(Long id, Integer id_externo, String direccion, Double latitud, Double longitud, Integer estado, String obs, Integer tipo_visita, String fecha, String empleado, String SID,
            Boolean geofenceActivate) {
        this.id = id;
        this.id_externo = id_externo;
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
        this.estado = estado;
        this.obs = obs;
        this.tipo_visita = tipo_visita;
        this.fecha = fecha;
        this.empleado = empleado;
        this.SID = SID;
        this.geofenceActivate = geofenceActivate;
    }


    public String getSID() {
        return SID;
    }

    public void setSID(String SID) {
        this.SID = SID;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Integer getTipo_visita() {
        return tipo_visita;
    }

    public void setTipo_visita(Integer tipo_visita) {
        this.tipo_visita = tipo_visita;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public Integer getId_externo() {
        return id_externo;
    }

    public void setId_externo(Integer id_externo) {
        this.id_externo = id_externo;
    }

    public static List<Reparaciones> getListaReparaciones(Activity actividad){
        ReparacionesDao rDao = ((App)actividad.getApplication()).getDaoSession().getReparacionesDao();
        List<Reparaciones> l = rDao.loadAll();
        return l;
    }

    public Boolean getGeofenceActivate() {
        return geofenceActivate;
    }

    public void setGeofenceActivate(Boolean geofenceActivate) {
        this.geofenceActivate = geofenceActivate;
    }

    public static void actualizarReparaciones(final Activity actividad, final SimpleItemRecyclerViewAdapter adaptador){

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(actividad);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setMessage(actividad.getText(R.string.actualizando));
        pDialog.setCancelable(false);
        pDialog.setMax(100);
        pDialog.show();

        String end_point = Utiles.obtenerEndPointGetAllReparaciones(actividad);
        VolleySingleton volley = VolleySingleton.getInstance(actividad);

        final List<Reparaciones> lista = adaptador.getListaReparaciones();

        volley.addToRequestQueue(
                new JsonArrayRequest(
                        Request.Method.GET,
                        end_point,
                        null,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {

                                    // si hay resultados borro la tabla reparaciones
                                    if( response.length() > 0 ){

                                        // limpiamos el recicle view
                                        int old_lista_len = lista.size(); // guardamos la cantidad de elementos de la lista
                                        lista.clear(); // la limpiamos
                                        adaptador.notifyItemRangeRemoved(0, old_lista_len); // y notificamos que borramos de elem 0 al list size

                                        // borro todas las reparaciones
                                        ReparacionesDao rDao = ((App)actividad.getApplication()).getDaoSession().getReparacionesDao();
                                        rDao.deleteAll();

                                        // recorro el arreglo
                                        for (int i = 0; i < response.length(); i++) {

                                            // recupero los obj de a uno
                                            JSONObject tmp = response.getJSONObject(i);
                                            Integer id_externo = tmp.getInt("id");
                                            String direccion = tmp.getString("title");
                                            Double latitud = tmp.getDouble("lat");
                                            Double longitud = tmp.getDouble("lon");

                                            JSONObject user_data = tmp.optJSONObject("user_data");
                                            Integer estado = user_data.getInt("estado"); // 0 - pendiente de atencion | 1 - en proceso | 2 - finalizado
                                            String obs = user_data.getString("obs");
                                            Integer tipo_visita = user_data.getInt("visita"); // 1 - visita | 2 - reparacion
                                            String fecha = user_data.getString("fecha");
                                            String empleado = user_data.getString("empleado");
                                            String SID = user_data.getString("SID");

                                            Reparaciones r =  new Reparaciones();
                                            r.setId_externo(id_externo);
                                            r.setDireccion(direccion);
                                            r.setLatitud(latitud);
                                            r.setLongitud(longitud);
                                            r.setObs(obs);
                                            r.setTipo_visita(tipo_visita);
                                            r.setEstado(estado);
                                            r.setFecha(fecha);
                                            r.setEmpleado(empleado);
                                            r.setSID(SID);
                                            rDao.save(r);

                                            // guardo repa en las listas
                                            lista.add(r);
                                        }

                                        // ordeno la lista de mayor a menor id
                                        Collections.sort(lista, new ReparacionesComparator());

                                        // notificamos al recycle view que le  insertmamos elementos del 0 al list size
                                        adaptador.notifyItemRangeInserted(0, lista.size());
                                        // actualizamos fecha update
                                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                                        Long hoy = timestamp.getTime();
                                        Utiles.setPreference("last_update",hoy.toString(),actividad);

                                        // actualizo mi lista de geofences
                                        ((MainActivity)actividad).obtenerListaGeofence(lista);
                                    }

                                    // cierro el progress dialog
                                    pDialog.dismiss();

                                } catch (JSONException e) {
                                    Log.e("Error", e.getMessage());
                                    pDialog.dismiss();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                    //
                                } else if (error instanceof AuthFailureError) {
                                    //
                                } else if (error instanceof ServerError) {
                                    //
                                } else if (error instanceof NetworkError) {
                                    //
                                } else if (error instanceof ParseError) {
                                    //
                                }

                                Log.e("Error Volley", error.getMessage());
                                pDialog.dismiss();
                            }
                        }

                ).setRetryPolicy(new DefaultRetryPolicy(35000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static void checkReparacion(final Activity actividad, final SimpleItemRecyclerViewAdapter adaptador, final Reparaciones reparacion){

        // cehequeo si hay conexion para acualizar
        if( !(Utiles.conectadoRedMovil(actividad)) && !(Utiles.conectadoWifi(actividad))){
            Toast t = Toast.makeText(actividad,actividad.getText(R.string.sin_conexion),Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
            t.show();
            return;
        }

        final ProgressDialog pg;
        pg = new ProgressDialog(actividad);
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage(actividad.getText(R.string.check_reparacion));
        pg.setCancelable(false);
        pg.setMax(100);
        pg.show();

        VolleySingleton volley = VolleySingleton.getInstance(actividad);

        String end_point = Utiles.getUrlWebBase();
        String poi_id = getPreference("pois_actual",actividad);
        String token= Utiles.getTOKEN();
        String url = end_point + poi_id+ "/pois/" + reparacion.getId_externo() + token;

        volley.addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.GET,
                        url,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {

                                    // recupero obj reparacion
                                    JSONObject tmp = response;
                                    JSONObject user_data = tmp.optJSONObject("user_data");
                                    Integer estado = user_data.getInt("estado"); // 0 - pendiente de atencion | 1 - en proceso | 2 - finalizado
                                    String empleado = user_data.getString("empleado");
                                    String SID = user_data.getString("SID");

                                    // cierro el progress dialog
                                    pg.dismiss();

                                    if( estado != 0 || ( !empleado.contentEquals("null") && !SID.contentEquals("null") ) ){

                                        String error_check = String.format(actividad.getText(R.string.check_error).toString(), empleado);

                                        AlertDialog.Builder alerta =  new AlertDialog.Builder(actividad);
                                        alerta.setMessage(error_check)
                                                .setTitle(R.string.alert_error_check_titulo)
                                                .setCancelable(false)
                                                .setPositiveButton(R.string.alert_btn_aceptar, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                        actualizarReparaciones(actividad,adaptador);
                                                    }
                                                });
                                        alerta.show();

                                    }else{
                                        actualizarReparacion(actividad,adaptador,reparacion);
                                    }

                                } catch (JSONException e) {
                                    Log.e("Error", e.getMessage());
                                    pg.dismiss();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                    //
                                } else if (error instanceof AuthFailureError) {
                                    //
                                } else if (error instanceof ServerError) {
                                    //
                                } else if (error instanceof NetworkError) {
                                    //
                                } else if (error instanceof ParseError) {
                                    //
                                }

                                Log.e("Error Volley", error.getMessage());
                                pg.dismiss();
                            }
                        }

                ).setRetryPolicy(new DefaultRetryPolicy(35000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }

    public static void actualizarReparacion(final Activity actividad, final SimpleItemRecyclerViewAdapter adaptador, final Reparaciones reparacion){

        // cehequeo si hay conexion para acualizar
        if( !(Utiles.conectadoRedMovil(actividad)) && !(Utiles.conectadoWifi(actividad))){
            Toast t = Toast.makeText(actividad,actividad.getText(R.string.sin_conexion),Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
            t.show();
            return;
        }

        final ProgressDialog pg;
        pg = new ProgressDialog(actividad);
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage(actividad.getText(R.string.asignar_reparacion));
        pg.setCancelable(false);
        pg.setMax(100);
        pg.show();

        VolleySingleton volley = VolleySingleton.getInstance(actividad);

        String end_point = Utiles.getUrlWebBase();
        String poi_id = getPreference("pois_actual",actividad);
        String token= Utiles.getTOKEN();
        String url = end_point + poi_id+ "/pois/" + reparacion.getId_externo() + token;

        // Utilizamos un array map cuando queremos enviar parametros a nuestro end point
        Map<String, String> jsonUserData = new HashMap<String, String>();
        jsonUserData.put("estado", "1");// 1 - En proceso
        jsonUserData.put("visita", reparacion.getTipo_visita().toString());
        jsonUserData.put("obs", reparacion.getObs());
        jsonUserData.put("fecha", reparacion.getFecha());
        jsonUserData.put("empleado", Utiles.getPreference("nombre",actividad));
        jsonUserData.put("SID", Utiles.getPreference("SID",actividad));

        Map<String, Object> jsonParams = new HashMap<String, Object>();
        jsonParams.put("title", reparacion.getDireccion());
        jsonParams.put("subtitle", "null");
        jsonParams.put("user_data", new JSONObject(jsonUserData) );

        JSONObject jobject = new JSONObject(jsonParams);

        // y depues tratamos de sincronizar
        volley.addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.PUT,
                        url,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {

                                    Log.i("VOLLEY Response", response.toString());

                                    // una ves que impacte los cambios en la api rest
                                    // los guardo en la db local
                                    final ReparacionesDao rDao = ((App)actividad.getApplication()).getDaoSession().getReparacionesDao();
                                    reparacion.setEstado(1);
                                    reparacion.setEmpleado(Utiles.getPreference("nombre",actividad));
                                    reparacion.setSID(Utiles.getPreference("SID",actividad));
                                    rDao.save(reparacion);

                                    // notifico al adaptador del cambio
                                    adaptador.notifyDataSetChanged();
                                    pg.dismiss();

                                    Reparaciones.notificarActualizacion(actividad,adaptador,reparacion);

                                } catch (Exception e) {
                                    Toast t = Toast.makeText(actividad,e.getMessage(),Toast.LENGTH_LONG);
                                    t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                                    t.show();
                                    pg.dismiss();
                                    Log.e("Error", e.getMessage());
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                    //
                                } else if (error instanceof AuthFailureError) {
                                    //
                                } else if (error instanceof ServerError) {
                                    //
                                } else if (error instanceof NetworkError) {
                                    //
                                } else if (error instanceof ParseError) {
                                    //
                                }

                                Toast t = Toast.makeText(actividad,error.getMessage(),Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                                t.show();
                                pg.dismiss();
                                Log.e("Error Volley", error.getMessage());
                            }
                        }

                ).setRetryPolicy(new DefaultRetryPolicy(35000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }

    public static void notificarActualizacion(final Activity actividad, final SimpleItemRecyclerViewAdapter adaptador, final Reparaciones reparacion){

        // cehequeo si hay conexion para acualizar
        if( !(Utiles.conectadoRedMovil(actividad)) && !(Utiles.conectadoWifi(actividad))){
            Toast t = Toast.makeText(actividad,actividad.getText(R.string.sin_conexion),Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
            t.show();
            return;
        }

        final ProgressDialog pg;
        pg = new ProgressDialog(actividad);
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage(actividad.getText(R.string.notificar_reparacion));
        pg.setCancelable(false);
        pg.setMax(100);
        pg.show();

        VolleySingleton volley = VolleySingleton.getInstance(actividad);
        String url = "https://fcm.googleapis.com/fcm/send";

        Map<String, String> jsonUserData = new HashMap<String, String>();
        jsonUserData.put("accion", "ACTUALIZAR");

        Map<String, Object> jsonParams = new HashMap<String, Object>();
        jsonParams.put("to", "/topics/TNT_FINAL");
        jsonParams.put("data", new JSONObject(jsonUserData) );


        JSONObject jobject = new JSONObject(jsonParams);

        // y depues tratamos de sincronizar
        volley.addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        url,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {

                                    Log.i("VOLLEY Response", response.toString());
                                    pg.dismiss();

                                } catch (Exception e) {
                                    Toast t = Toast.makeText(actividad,e.getMessage(),Toast.LENGTH_LONG);
                                    t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                                    t.show();
                                    pg.dismiss();
                                    Log.e("Error", e.getMessage());
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                    //
                                } else if (error instanceof AuthFailureError) {
                                    //
                                } else if (error instanceof ServerError) {
                                    //
                                } else if (error instanceof NetworkError) {
                                    //
                                } else if (error instanceof ParseError) {
                                    //
                                }

                                Toast t = Toast.makeText(actividad,error.getMessage(),Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                                t.show();
                                pg.dismiss();
                                Log.e("Error Volley", error.getMessage());
                            }
                        }

                ){
                    /** Passing some request headers* */
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "key=AAAACVIhTKI:APA91bHRXWKD_x36a10CEyOKgVzxu33Kb5I_7hgyQ6nK3qeMiA7_sEWfMC-OlG1OtH0AN3JOZEmgbiDAilh5snux5QV3xeHSdp6b7_FabC7IDnjJpw0YYoM3E25WIADEEqpfXB2PCFk_");
                        return headers;
                    }
                }
                .setRetryPolicy(new DefaultRetryPolicy(35000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));

    }

    public void finalizarReparacion(final Activity actividad, final SimpleItemRecyclerViewAdapter adaptador){

        final Reparaciones reparacion =  this;
        final ProgressDialog pg;
        pg = new ProgressDialog(actividad);
        pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pg.setMessage(actividad.getText(R.string.finalizar_reparacion));
        pg.setCancelable(false);
        pg.setMax(100);
        pg.show();

        VolleySingleton volley = VolleySingleton.getInstance(actividad);

        String end_point = Utiles.getUrlWebBase();
        String poi_id = getPreference("pois_actual",actividad);
        String token= Utiles.getTOKEN();
        String url = end_point + poi_id+ "/pois/" + reparacion.getId_externo() + token;

        // Utilizamos un array map cuando queremos enviar parametros a nuestro end point
        Map<String, String> jsonUserData = new HashMap<String, String>();
        jsonUserData.put("estado", reparacion.getEstado().toString());// 2 - Finalizado
        jsonUserData.put("visita", reparacion.getTipo_visita().toString());
        jsonUserData.put("obs", reparacion.getObs());
        jsonUserData.put("fecha", reparacion.getFecha());
        jsonUserData.put("empleado", reparacion.getEmpleado());
        jsonUserData.put("SID", reparacion.getSID());

        Map<String, Object> jsonParams = new HashMap<String, Object>();
        jsonParams.put("title", reparacion.direccion);
        jsonParams.put("subtitle", "-");
        jsonParams.put("user_data", new JSONObject(jsonUserData) );

        JSONObject jobject = new JSONObject(jsonParams);

        volley.addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.PUT,
                        url,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {

                                    // una ves que ya modioficamos en la api rest
                                    // impactamos sobre nuestra db
                                    ReparacionesDao rDao = ((App)actividad.getApplication()).getDaoSession().getReparacionesDao();
                                    rDao.save(reparacion);

                                    Log.i("VOLLEY2 Response", response.toString());
                                    pg.dismiss();

                                    AlertDialog.Builder alerta =  new AlertDialog.Builder(actividad);
                                    alerta.setMessage(R.string.alert_msg)
                                            .setTitle(R.string.alert_titulo)
                                            .setCancelable(false)
                                            .setPositiveButton(R.string.alert_btn_aceptar, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();

                                                    //Enviar mensaje Firebase
                                                    notificarActualizacion(actividad,adaptador,reparacion);

                                                    // si es != de null es por que estoy en una tablet
                                                    if (actividad.findViewById(R.id.item_detail_container) != null) {

                                                        // notifico que cambiaron los elementos del adaptador
                                                        if( adaptador != null )
                                                            adaptador.notifyDataSetChanged();

                                                        // inicializo el mapa en el fragment detalles con todos los puntos
                                                        DetalleFragment fragment = new DetalleFragment();
                                                        ((MainActivity)actividad).getSupportFragmentManager().beginTransaction().replace(R.id.item_detail_container, fragment).commit();
                                                    }else {
                                                        // estoy en un celular
                                                        // lanzo actividad principal
                                                        Intent i = new Intent();
                                                        i.setClass(actividad.getApplicationContext(), MainActivity.class);
                                                        actividad.startActivity(i);
                                                        actividad.finish();
                                                    }

                                                }
                                            });
                                    alerta.show();


                                } catch (Exception e) {
                                    Toast t = Toast.makeText(actividad,e.getMessage(),Toast.LENGTH_LONG);
                                    t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                                    t.show();
                                    pg.dismiss();
                                    Log.e("Error", e.getMessage());
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                    //
                                } else if (error instanceof AuthFailureError) {
                                    //
                                } else if (error instanceof ServerError) {
                                    //
                                } else if (error instanceof NetworkError) {
                                    //
                                } else if (error instanceof ParseError) {
                                    //
                                }
                                Toast t = Toast.makeText(actividad,error.getMessage(),Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                                t.show();
                                pg.dismiss();
                                Log.e("Error Volley", error.getMessage());
                            }
                        }

                ).setRetryPolicy(new DefaultRetryPolicy(35000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
    }
}
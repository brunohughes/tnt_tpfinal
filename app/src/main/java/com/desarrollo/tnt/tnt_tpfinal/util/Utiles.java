package com.desarrollo.tnt.tnt_tpfinal.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Utiles {

    public static String URL_WEB_BASE = "https://poi.wrld3d.com/v1.1/poisets/";
    public static String TOKEN = "c375caacd06bd1f24dfcba6f18ae1c7254726d16c3f2b38799ce7a0dc8d20dfb5f52594bec5da56f";
    public static Long TIME_UPDATE = (300000L); // 1 hora = 3.600.000 milisegundos // 300000 = 5 min

    public static Integer NOTIFICATION_ID = 2013092902;
    public static String FCM_TEMA = "TNT_FINAL";
    public static String ANDROID_CHANNEL_ID = "com.desarrollo.tnt.tnt_tpfinal.TNT_FINAL";
    public static String ANDROID_CHANNEL_NAME = "TNT FINAL";
    public static String ANDROID_CHANNEL_DESC = "Canal de notificaciones TNT FINAL";

    // geofence
    public static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 70;


    public static void crearCanalNotificacion(Context contexto) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = ANDROID_CHANNEL_NAME;
            String description = ANDROID_CHANNEL_DESC;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(ANDROID_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = contexto.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static String obtenerEndPointGetAllReparaciones(Context contexto){
        String pois_actual = getPreference("pois_actual",contexto);
        if( pois_actual != null ){
            return URL_WEB_BASE + pois_actual + "/pois/?token=" + TOKEN;
        }
        return null;
    }

    public static String getUrlWebBase(){
            return URL_WEB_BASE;
    }

    public static String getTOKEN(){
        return "?token="+TOKEN;
    }

    public static Boolean conectadoWifi(Context contexto){
        ConnectivityManager connectivity = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean conectadoRedMovil(Context contexto){
        ConnectivityManager connectivity = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    // formato yyyy-MM-dd o dd-MM-yyyy o yyyy-MM-dd HH:mm:ss o dd-MM-yyyy HH:mm:ss etc
    public static String fechaHoraDeHoy(String formato){
        Date fecha = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat(formato);
        String hoy = formateador.format(fecha).toString();
        return hoy;
    }
    public static void setPreference(String nombre, String valor, Context cont){

        SharedPreferences sp =  cont.getSharedPreferences("APP_TNT", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(nombre,valor);
        editor.apply();
    }

    public static String getPreference(String nombre, Context cont){

        SharedPreferences sp = cont.getSharedPreferences("APP_TNT", Context.MODE_PRIVATE);

        if (sp.contains(nombre))
        {
            return sp.getString(nombre,"");
        }

        return null;
    }

    public static void alertaR(int msg,int titulo, int btnTxt, Context contex){

        AlertDialog.Builder alerta =  new AlertDialog.Builder(contex);

        alerta.setMessage(msg)
                .setTitle(titulo)
                .setPositiveButton(btnTxt, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alerta.show();
    }

    public static void alertaTxt(String msg,String titulo, String btnTxt, Context contex){

        AlertDialog.Builder alerta =  new AlertDialog.Builder(contex);

        alerta.setMessage(msg)
                .setTitle(titulo)
                .setPositiveButton(btnTxt, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alerta.show();
    }

    public static void mensajeConFormato(TextView titulo, TextView msg, Context contex){

        DialogInterface.OnClickListener onCLick = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        };

        AlertDialog.Builder alerta =  new AlertDialog.Builder(contex);

        alerta.setView(msg)
                .setCustomTitle(titulo)
                .setPositiveButton("Aceptar",onCLick)
                .setCancelable(true);

        AlertDialog dialog = alerta.create();
        dialog.show();

    }

    public static String formatFechaHora(String strFecha){

        String[] t = strFecha.split(" ");
        String[] t2 = t[0].split("-");
        return t2[2] + "/" + t2[1] + "/" + t2[0];
    }

    public static String sha512(String clear) {

        String sha512 =  null;

        try{

            MessageDigest md = MessageDigest.getInstance("sha-512");
            byte[] b = md.digest(clear.getBytes());

            int size = b.length;
            StringBuffer h = new StringBuffer(size);

            for (int i = 0; i < size; i++) {

                int u = b[i] & 255;

                if (u < 16) {
                    h.append("0" + Integer.toHexString(u));
                } else {
                    h.append(Integer.toHexString(u));
                }
            }

            sha512 = h.toString();

        }catch(Exception e){
            Log.e("ERROR sha512",e.getMessage());
        }

        return sha512;
    }



    public static String md5(String clear) {

        String md5 =  null;

        try{

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] b = md.digest(clear.getBytes());

            int size = b.length;
            StringBuffer h = new StringBuffer(size);

            for (int i = 0; i < size; i++) {

                int u = b[i] & 255;

                if (u < 16) {
                    h.append("0" + Integer.toHexString(u));
                } else {
                    h.append(Integer.toHexString(u));
                }
            }

            md5 = h.toString();

        }catch(Exception e){
            Log.e("ERROR MD5",e.getMessage());
        }

        return md5;
    }
}
package com.desarrollo.tnt.tnt_tpfinal.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.desarrollo.tnt.tnt_tpfinal.DetalleActivity;
import com.desarrollo.tnt.tnt_tpfinal.DetalleFragment;
import com.desarrollo.tnt.tnt_tpfinal.MainActivity;
import com.desarrollo.tnt.tnt_tpfinal.ObservacionesActivity;
import com.desarrollo.tnt.tnt_tpfinal.ObservacionesFragment;
import com.desarrollo.tnt.tnt_tpfinal.R;
import com.desarrollo.tnt.tnt_tpfinal.clases.ReparacionesComparator;
import com.desarrollo.tnt.tnt_tpfinal.modelos.Reparaciones;
import com.desarrollo.tnt.tnt_tpfinal.util.Utiles;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> implements Serializable {

    private final MainActivity mParentActivity;
    private final List<Reparaciones> mValues;
    private final boolean mTwoPane;
    private final SimpleItemRecyclerViewAdapter adaptador;
    private Intent intent;

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Reparaciones item = (Reparaciones) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putSerializable("reparacion", item);
                DetalleFragment fragment = new DetalleFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, DetalleActivity.class);
                intent.putExtra("reparacion", item);
                context.startActivity(intent);
            }
        }
    };

    public SimpleItemRecyclerViewAdapter(MainActivity parent, List<Reparaciones> items, boolean twoPane) {

        // ordeno la lista de mayor a menor
        this.mValues = items;
        Collections.sort(this.mValues, new ReparacionesComparator());

        this.mParentActivity = parent;
        this.mTwoPane = twoPane;
        this.adaptador = this;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String txt_fecha = String.format(mParentActivity.getText(R.string.txt_fecha).toString(),mValues.get(position).getFecha());
        String txt_direccion = String.format(mParentActivity.getText(R.string.txt_direccion).toString(),mValues.get(position).getDireccion());

        String tLat = mValues.get(position).getLatitud().toString().substring(0,10);
        String tLong = mValues.get(position).getLongitud().toString().substring(0,10);
        String txt_lat_long = String.format(mParentActivity.getText(R.string.txt_lat_long).toString(),tLat,tLong);

        holder.id_fecha.setText(txt_fecha);
        holder.id_direccion.setText(txt_direccion);
        holder.id_lat_long.setText(txt_lat_long);

        switch ( mValues.get(position).getEstado() ){
            case 0:
                Glide.with(mParentActivity).load(R.drawable.estado0).into(holder.id_img_estado);
                break;

            case 1:
                Glide.with(mParentActivity).load(R.drawable.estado1).into(holder.id_img_estado);
                break;

            case 2:
                Glide.with(mParentActivity).load(R.drawable.estado2).into(holder.id_img_estado);
                break;
        }

        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);

        // seteo los botones d ela interfaz
        Integer estado = mValues.get(position).getEstado();

        // identificador de quien se hizo cargo de la reparacion
        String SIDR = mValues.get(position).getSID();
        // identificador dal actual reparador
        String SID = Utiles.getPreference("SID",mParentActivity);

        if( estado == 0 ){
            // estado pendiente
            // solo boton tomar repa
            holder.btn_tomar_reparacion.setVisibility(View.VISIBLE);
            holder.btn_finalizar_reparacion.setVisibility(View.GONE);
            holder.id_estado.setText(R.string.estado_0);
            holder.v.setBackgroundColor(holder.v.getResources().getColor(R.color.colorBgReparacion2));
        }

        if( estado == 1 ){
            // estado en proceso
            // solo boton finalizar repa

            holder.btn_tomar_reparacion.setVisibility(View.GONE);
            holder.btn_finalizar_reparacion.setVisibility(View.VISIBLE);
            // como la reparacion esta en proceso
            // solo voy a mostrar el boton de finalizar
            // si la reparacion es mia
            if( SIDR.contentEquals(SID) ){
                holder.v.setBackgroundColor(holder.v.getResources().getColor(R.color.colorBgReparacion1));
                holder.btn_finalizar_reparacion.setText(R.string.btn_txt_estado_1);
                holder.btn_finalizar_reparacion.setEnabled(true);
            }else{
                holder.v.setBackgroundColor(holder.v.getResources().getColor(R.color.colorBgReparacion2));
                holder.btn_finalizar_reparacion.setText(mValues.get(position).getEmpleado());
                holder.btn_finalizar_reparacion.setEnabled(false);
            }

            holder.id_estado.setText(R.string.estado_1);
        }

        if( estado == 2 ){
            // estado finalizado
            // solo el boton de finalizar con el nombre de quien la hizo
            holder.btn_tomar_reparacion.setVisibility(View.GONE);
            holder.btn_finalizar_reparacion.setVisibility(View.VISIBLE);
            holder.btn_finalizar_reparacion.setText(mValues.get(position).getEmpleado());
            holder.btn_finalizar_reparacion.setEnabled(false);
            holder.id_estado.setText(mParentActivity.getText(R.string.estado_2));

            if( SIDR.contentEquals(SID) ){
                holder.v.setBackgroundColor(holder.v.getResources().getColor(R.color.colorBgReparacion1));
            }else{
                holder.v.setBackgroundColor(holder.v.getResources().getColor(R.color.colorBgReparacion2));
            }
        }

        holder.btn_tomar_reparacion.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Reparaciones.checkReparacion(mParentActivity,adaptador,mValues.get(position));
                //Enviar mensaje Firebase
               // Reparaciones.notificarActualizacion(mParentActivity,adaptador,mValues.get(position));
            }
        });

        holder.btn_finalizar_reparacion.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                // cehequeo si hay conexion para acualizar
                if( !(Utiles.conectadoRedMovil(mParentActivity)) && !(Utiles.conectadoWifi(mParentActivity))){
                    Toast t = Toast.makeText(mParentActivity,mParentActivity.getText(R.string.sin_conexion),Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                    t.show();
                    return;
                }
                // si hay dos paneles cargo el fragmento
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("reparacion", mValues.get(position));
                    arguments.putSerializable("adaptador", adaptador);

                    ObservacionesFragment fragment = new ObservacionesFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    // sino lanzo la actividad
                    Context context = mParentActivity;
                    intent = new Intent(context, ObservacionesActivity.class);
                    intent.putExtra("reparacion", mValues.get(position));
                    context.startActivity(intent);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public List<Reparaciones> getListaReparaciones(){
        return this.mValues;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView id_fecha;
        final TextView id_direccion;
        final ImageView id_img_estado;
        final TextView id_lat_long;
        final Button btn_tomar_reparacion;
        final Button btn_finalizar_reparacion;
        final TextView id_estado;
        final View v;

        ViewHolder(View view) {
            super(view);
            id_fecha = (TextView) view.findViewById(R.id.id_fecha);
            id_direccion = (TextView) view.findViewById(R.id.id_direccion);
            id_img_estado = (ImageView) view.findViewById(R.id.id_imagen_estado);
            id_lat_long = (TextView) view.findViewById(R.id.id_lat_long);
            btn_tomar_reparacion = view.findViewById(R.id.buttonTomarReparacion);
            btn_finalizar_reparacion = view.findViewById(R.id.buttonFinalizarReparacion);
            id_estado = (TextView) view.findViewById(R.id.id_estado);
            v = view;
        }
    }
}
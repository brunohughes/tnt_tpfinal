package com.desarrollo.tnt.tnt_tpfinal.adaptadores;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.desarrollo.tnt.tnt_tpfinal.R;
import com.desarrollo.tnt.tnt_tpfinal.modelos.Reparaciones;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private LayoutInflater inflater;
    private Reparaciones reparacion;

    public CustomInfoWindowAdapter(LayoutInflater inflater, Reparaciones reparacion){
        this.inflater = inflater;
        this.reparacion = reparacion;
    }

    @Override
    public View getInfoContents(final Marker m) {
        //Carga layout personalizado.
        View v = inflater.inflate(R.layout.infowindow_layout, null);

        ((TextView)v.findViewById(R.id.info_direccion)).setText(String.format(v.getResources().getText(R.string.info_direccion_txt).toString(),reparacion.getDireccion()));

        if( !reparacion.getEmpleado().contentEquals("null") ){
            ((TextView)v.findViewById(R.id.info_empleado)).setText(String.format(v.getResources().getText(R.string.info_empleado_txt).toString(),reparacion.getEmpleado()));
        }else {
            ((TextView) v.findViewById(R.id.info_empleado)).setVisibility(View.GONE);
        }

        String estado_txt = "";
        switch (reparacion.getEstado()){
            case 0:
                // estado pendiente
                estado_txt = "Pendiente de atención";
                break;

            case 1:
                // estado en proceso
                estado_txt = "En proceso";
                break;

            case 2:
                // finalizado
                estado_txt = "Finallizada";
                break;
        }
        ((TextView)v.findViewById(R.id.info_estado)).setText(String.format(v.getResources().getText(R.string.info_estado_txt).toString(),estado_txt));
        ((TextView)v.findViewById(R.id.info_obs)).setText(String.format(v.getResources().getText(R.string.info_obs_txt).toString(),reparacion.getObs()));

        
        return v;
    }

    @Override
    public View getInfoWindow(Marker m) {
        return null;
    }

}
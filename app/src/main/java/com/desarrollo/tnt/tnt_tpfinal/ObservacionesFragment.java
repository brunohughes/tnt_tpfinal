package com.desarrollo.tnt.tnt_tpfinal;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.desarrollo.tnt.tnt_tpfinal.adaptadores.SimpleItemRecyclerViewAdapter;
import com.desarrollo.tnt.tnt_tpfinal.modelos.Reparaciones;
import com.desarrollo.tnt.tnt_tpfinal.util.Utiles;


/**
 * A simple {@link Fragment} subclass.
 */
public class ObservacionesFragment extends Fragment {

    private Button btn_enviar;
    private EditText obs;
    private Spinner tipo;
    private Reparaciones reparacion = null;
    private SimpleItemRecyclerViewAdapter adaptador = null;

    public ObservacionesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_observaciones, container, false);
        inicializarUI(v);

        reparacion = ( getArguments() != null ) ? (Reparaciones) getArguments().getSerializable("reparacion") : null;
        adaptador = ( getArguments() != null ) ? (SimpleItemRecyclerViewAdapter) getArguments().getSerializable("adaptador") : null;

        return v;
    }

    private void inicializarUI(View v){

        tipo = v.findViewById(R.id.spinner_tipo_visita);
        obs = v.findViewById(R.id.obs_msg);
        btn_enviar = v.findViewById(R.id.btn_enviar_obs);

        btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( reparacion != null ){

                    // cehequeo si hay conexion para acualizar
                    if( !(Utiles.conectadoRedMovil(getActivity())) && !(Utiles.conectadoWifi(getActivity()))){
                        Toast t = Toast.makeText(getActivity(),getActivity().getText(R.string.sin_conexion),Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                        t.show();
                        return;
                    }

                    String obs_txt = obs.getText().toString();
                    Integer tipo_v = tipo.getSelectedItemPosition();

                    if( !TextUtils.isEmpty(obs_txt) && tipo_v != null ){
                        tipo_v =( tipo.getSelectedItemPosition() == 0 ) ? 1 : 2;

                        // guardo lo que ya esta en obs
                        String obs = reparacion.getObs() +"\nObs: " + obs_txt;

                        reparacion.setObs(obs);
                        reparacion.setTipo_visita(tipo_v);
                        reparacion.setEstado(2);
                        reparacion.finalizarReparacion(getActivity(),adaptador);
                    }else{
                        Toast t = Toast.makeText(getContext(),R.string.error_enviar_obs_campos,Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                        t.show();
                    }
                }
            }
        });
    }
}
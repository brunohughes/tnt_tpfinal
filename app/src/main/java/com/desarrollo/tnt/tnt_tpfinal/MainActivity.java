package com.desarrollo.tnt.tnt_tpfinal;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.desarrollo.tnt.tnt_tpfinal.adaptadores.SimpleItemRecyclerViewAdapter;
import com.desarrollo.tnt.tnt_tpfinal.geofence.GeofenceBroadcastReceiver;
import com.desarrollo.tnt.tnt_tpfinal.modelos.DaoSession;
import com.desarrollo.tnt.tnt_tpfinal.modelos.Reparaciones;
import com.desarrollo.tnt.tnt_tpfinal.modelos.ReparacionesDao;
import com.desarrollo.tnt.tnt_tpfinal.modelos.Usuario;
import com.desarrollo.tnt.tnt_tpfinal.util.App;
import com.desarrollo.tnt.tnt_tpfinal.util.Utiles;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.greenrobot.greendao.query.QueryBuilder;

import java.sql.Array;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.location.Geofence.NEVER_EXPIRE;

public class MainActivity extends AppCompatActivity {

    private boolean mTwoPane;
    private final Context contexto = this;
    private SimpleItemRecyclerViewAdapter adaptador;
    private List<Reparaciones> listaReparaciones;

    // geofence
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private GeofencingClient mGeofencingClient;
    private ArrayList<Geofence> mGeofenceList;
    private PendingIntent mGeofencePendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        // suscribo la app a un topico general
        if( Utiles.getPreference("suscripto",contexto) == null ){
            FirebaseMessaging.getInstance().subscribeToTopic(Utiles.FCM_TEMA);
            Utiles.setPreference("suscripto","si",contexto);
        }

        // creo los canales de notificacion
        Utiles.crearCanalNotificacion(contexto);

        // guardo el id del poset que utilizamos
        Utiles.setPreference("pois_actual","5915",contexto);

        // chequear si hay usario de la app registrado
        if(Usuario.checkUsuario(this) == false ){
            // lanzo actividad para crear un usuario en la base de datos
            Intent i = new Intent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.setClass(MainActivity.this, LoginActivity.class);
            startActivity(i);
            // finalizo esta actividad, de manera que quede solo la nueva actividad que lance
            this.finish();
        }else{

            // geofence
            mGeofenceList = new ArrayList<>();
            mGeofencePendingIntent = null;
            mGeofencingClient = LocationServices.getGeofencingClient(this);

            RecyclerView recyclerView = findViewById(R.id.item_list);
            assert recyclerView != null;

            // inicializo lista de reparaciones y el adaptador
            listaReparaciones = Reparaciones.getListaReparaciones(this);
            adaptador = new SimpleItemRecyclerViewAdapter(this, listaReparaciones, mTwoPane);
            recyclerView.setAdapter(adaptador);

            // manejo de notificaciones
            notificaciones(getIntent());

            // chequeo la ultima ves que actualice las reparaciones
            checkLastUpdateReparaciones(contexto);

            if (findViewById(R.id.item_detail_container) != null) {
                mTwoPane = true;
                // inicializo el mapa en el fragment detalles con todos los puntos
                DetalleFragment fragment = new DetalleFragment();
                this.getSupportFragmentManager().beginTransaction().replace(R.id.item_detail_container, fragment).commit();
            }
        }
    }

    public void notificaciones(Intent intent){

        if( intent.getAction() == null )
            return;

        if( intent.getAction().equals("ACTUALIZAR_FCM") ) {
            this.actualizar();
            setIntent(null);
        }

        if( intent.getAction().equals("REPARACION_CERCANA") ){
            Integer id = intent.getIntExtra("id", -1);

            if( id != -1 ){

                ReparacionesDao rDao = ((App)getApplication()).getDaoSession().getReparacionesDao();
                Reparaciones item = rDao.queryBuilder().where(ReparacionesDao.Properties.Id_externo.eq(id)).unique();

                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("reparacion", item);
                    DetalleFragment fragment = new DetalleFragment();
                    fragment.setArguments(arguments);
                    this.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Intent i = new Intent(contexto, DetalleActivity.class);
                    i.putExtra("reparacion", item);
                    startActivity(i);
                }

            }
        }

    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        notificaciones(intent);
    }


    private void checkLastUpdateReparaciones(Context contexto){

        String last_update = Utiles.getPreference("last_update",contexto);
        Boolean actualizar = false;

        if( last_update == null ){
            actualizar = true;
        }else{
            // guardo los segundos actuales
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            Long hoy = timestamp.getTime();
            Long last = Long.valueOf(last_update);

            Log.i("Tiempo Update", "Pasaron: " + (hoy-last) + " Limite: " + Utiles.TIME_UPDATE);

            // si la diferencia es mayor que el tiempo de actualizacion
            if( ( hoy - last ) > Utiles.TIME_UPDATE )
               actualizar = true;
        }
        // actualizo si es necesario
        if (actualizar) {
            this.actualizar();
        }
    }

    public void actualizar(){

        // cehequeo si hay conexion para acualizar
        if( !(Utiles.conectadoRedMovil(contexto)) && !(Utiles.conectadoWifi(contexto))){
            Toast t = Toast.makeText(contexto,contexto.getText(R.string.sin_conexion),Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
            t.show();
            return;
        }

        Reparaciones.actualizarReparaciones(this,adaptador);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_actualizar) {
            actualizar();
        }

        if (id == R.id.action_ver_todo) {
            if (mTwoPane) {
                DetalleFragment fragment = new DetalleFragment();
                this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            } else {
                Intent intent = new Intent(contexto, DetalleActivity.class);
                contexto.startActivity(intent);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adaptador.notifyDataSetChanged();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        adaptador.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            requestPermissions();
        }

        adaptador.notifyDataSetChanged();
    }


    // geofence
    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceBroadcastReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permisos ok
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * This sample hard codes geofence data. A real app might dynamically create geofences based on
     * the user's location.
     */
    @SuppressLint("MissingPermission")
    public void obtenerListaGeofence(List<Reparaciones> reparaciones ) {

        for (Reparaciones entry : reparaciones) {

            // solo creo geofences a las reparaciones
            // que estan en estado pendiente
            if (entry.getEstado() != 0)
                continue;

            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(entry.getId_externo().toString())

                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            entry.getLatitud(),
                            entry.getLongitud(),
                            Utiles.GEOFENCE_RADIUS_IN_METERS
                    )

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transitGeofencingRequest ion. We track entry and exit transitions in this sample.
                    .setTransitionTypes( Geofence.GEOFENCE_TRANSITION_ENTER )//| Geofence.GEOFENCE_TRANSITION_EXIT)

                    // Create the geofence.
                    .build());
        }

        Log.i("geofence size", mGeofenceList.size() + "");

        if (mGeofenceList.size() > 0) {
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            // Geofences added
                            Log.i("geofence add ", "adddd");
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Failed to add geofences
                            Log.i("geofence add ", "faill");
                        }
                    });
            }
        }

}
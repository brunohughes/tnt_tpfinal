package com.desarrollo.tnt.tnt_tpfinal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.desarrollo.tnt.tnt_tpfinal.modelos.Reparaciones;

public class ObservacionesActivity extends AppCompatActivity {

    private Reparaciones reparacion = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observaciones);

        Bundle b = getIntent().getExtras();
        reparacion = (Reparaciones) b.getSerializable("reparacion");

        Bundle arguments = new Bundle();
        arguments.putSerializable("reparacion", reparacion);
        arguments.putSerializable("adaptador", null);
        ObservacionesFragment fragment = new ObservacionesFragment();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.item_detail_container_obs, fragment)
                .commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

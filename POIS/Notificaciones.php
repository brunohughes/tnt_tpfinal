<?php
class Notificaciones extends oNotificaciones {


	public static function notificacionFCM($notificacion){
		
		
		// api key
		// AAAACVIhTKI:APA91bHRXWKD_x36a10CEyOKgVzxu33Kb5I_7hgyQ6nK3qeMiA7_sEWfMC-OlG1OtH0AN3JOZEmgbiDAilh5snux5QV3xeHSdp6b7_FabC7IDnjJpw0YYoM3E25WIADEEqpfXB2PCFk_
		
		
		// topic
		// "TNT_FINAL"
		
		
		// data que enviamos
		// {"to":"/topics/TNT_FINAL","data":{"accion":"ACTUALIZAR"}}
		
		// recupero config 
		$fcm_api_key = Configuraciones::filter("nombre == 'FCM_API_KEY'");
		$fcm_topic = Configuraciones::filter("nombre == 'FCM_TOPIC'");
		
		$texto = utf8_encode($notificacion->getTexto());
		$texto = html_entity_decode($texto, ENT_COMPAT, 'UTF-8'); 
		
		$titulo = utf8_encode($notificacion->getTitulo());
		$titulo = html_entity_decode($titulo, ENT_COMPAT, 'UTF-8'); 
			
		$data = array (
			'id_externo' => $notificacion->getId(),
			'titulo' =>$titulo,
			'texto' => $texto,
			'fecha_envio' => $notificacion->getFecha_envio(),
			'imagen' => $notificacion->getImagen(),
			'priority' => 'high',
			'tipo' => "NOTIFICACION"
		);
		
		
		$fields = array('to' => '/topics/'.$fcm_topic->getValor(), 'data' => $data); 

		$headers = array
		(
		'Authorization: key=' . $fcm_api_key->getValor(),
		'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
		
		// paso la respuesta a json		
		$response = json_decode($result, true);
		
		// miro el resultado
		if( isset( $response['message_id'] ) ){
			$notificacion->setId_notificacion($response['message_id']);
			$notificacion->save();
			return "Notificación enviada con exito al los servidores FCM, Id de la Notificación: ".$response['message_id'];
				
		}else{
			// guardo el mensaje de error
			$error = ( isset($response["results"][0]["error"]) ) ? "No se envio la Notificación, Error: " . $response["results"][0]["error"] : "Error al enviar la Notificación a los servidores FCM, por favor trate de reenviar nuevamente en unos minutos" ;
			return $error;
		}
		
	}


	public static function notificacionProductoFCM($producto){
		
		// recupero config 
		$fcm_api_key = Configuraciones::filter("nombre == 'FCM_API_KEY'");
		$fcm_topic = Configuraciones::filter("nombre == 'FCM_TOPIC'");
						
		$categoria = utf8_encode($producto->getCategoria());
		$categoria = html_entity_decode($categoria, ENT_COMPAT, 'UTF-8'); 
		
		$codigo = utf8_encode($producto->getCodigo());
		$codigo = html_entity_decode($codigo, ENT_COMPAT, 'UTF-8'); 
				
		$categoria_nombre = utf8_encode($producto->getCategoria()->getNombre());
		$categoria_nombre = html_entity_decode($categoria_nombre, ENT_COMPAT, 'UTF-8'); 
		
		$titulo = utf8_encode($producto->getTitulo());
		$titulo = html_entity_decode($titulo, ENT_COMPAT, 'UTF-8'); 
		
		$descripcion = utf8_encode($producto->getDescripcion());
		$descripcion = html_entity_decode($descripcion, ENT_COMPAT, 'UTF-8'); 

		$imagenesRel = $producto->getImagenesRel();
		$imagenesRel = ( is_array($imagenesRel) ) ? $imagenesRel : array($imagenesRel);
		
		$imagenes =  array();
		
		foreach( $imagenesRel as $key => $i){
			$imagenes[$key] = $i->getUrl();
		}
		
		$data = array (
		
			'id_externo' => $producto->getId(),
			'nombre_categoria' => $categoria_nombre,
			'categoria' => $categoria,
			'codigo' => $codigo,
			'titulo' => $titulo,
			'descripcion' => $descripcion,
			'imagen' => $producto->getImagen(),
		
			'precio_minorista' => Productos::getPrecio_real($producto->getPrecio_minorista()),
			'precio_mayorista' => Productos::getPrecio_real($producto->getPrecio_mayorista()),
		
			'oferta_precio_minorista' => Productos::getPrecio_real($producto->getOferta_precio_minorista()),
			'oferta_minorista' => $producto->getOferta_minorista(),
			'oferta_cantidad_minorista' => $producto->getOferta_cantidad_minorista(),
			
			'oferta_precio_mayorista' => Productos::getPrecio_real($producto->getOferta_precio_mayorista()),
			'oferta_mayorista' => $producto->getOferta_mayorista(),
			'oferta_cantidad_mayorista' => $producto->getOferta_cantidad_mayorista(),
			
			
			'fecha_creacion' => $producto->getFecha_creacion(),
			'fecha_update' => $producto->getFecha_update(),
			
			'destacado' => $producto->getDestacado(),
			'activo' => $producto->getActivo(),
			
			'tipo' => "PRODUCTO_ID",
			'imagenes' => $imagenes 
		);
		
		$fields = array('to' => '/topics/'.$fcm_topic->getValor(), 'data' => $data); 

		$headers = array
		(
		'Authorization: key=' . $fcm_api_key->getValor(),
		'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
		
		// paso la respuesta a json		
		$response = json_decode($result, true);
		
		// miro el resultado
		if( isset( $response['message_id'] ) ){
			return "Notificación enviada con exito al los servidores FCM, Id de la Notificación: ".$response['message_id'];
				
		}else{
			// guardo el mensaje de error
			$error = ( isset($response["results"][0]["error"]) ) ? "No se envio la Notificación, Error: " . $response["results"][0]["error"] : "Error al enviar la Notificación a los servidores FCM, por favor trate de reenviar nuevamente en unos minutos" ;
			return $error;
		}
		
	}

}
?>
